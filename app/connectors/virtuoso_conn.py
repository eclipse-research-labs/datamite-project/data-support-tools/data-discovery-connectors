#-------------------------------------------------------------------------------
# Copyright (C) 2024 ITI
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#-------------------------------------------------------------------------------

# Ensure to import only the necessary library for connecting to the storage solution selected
import subprocess
try:
    from SPARQLWrapper import SPARQLWrapper, JSON
except ImportError:
    subprocess.run(["pip", "install", "sparqlwrapper"])
    from SPARQLWrapper import SPARQLWrapper, JSON


###
# Connect to Virtuoso based on config_conn params and return metadata_obj
###
def db_conn(config_conn):
    try:
        print("-> Trying to connect to Virtuoso...")

        sparql = SPARQLWrapper(config_conn["endpoint"])
        sparql.setReturnFormat(JSON)

        # Example SPARQL query to retrieve metadata information
        sparql.setQuery("""
            select ?g where { graph ?g {?s ?p ?o} }
        """)

        results = sparql.query().convert()

        metadata_obj = []

        for result in results["results"]["bindings"]:
            metadata_info = {
                "graph": result["g"]["value"]
            }
            metadata_obj.append(metadata_info)

        print("-> Metadata info of Virtuoso collected.")

    except Exception as error:
        print("-> Error while connecting to Virtuoso:", error)
        metadata_obj = []

    return metadata_obj

