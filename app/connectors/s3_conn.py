#-------------------------------------------------------------------------------
# Copyright (C) 2024 ITI
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#-------------------------------------------------------------------------------

# Ensure to import only the necessary library for connecting to the storage solution selected
import subprocess
try:
    import boto3
except ImportError:
    subprocess.run(["pip", "install", "boto3"])
    import boto3


def db_conn(config_conn):
    try:
        print("-> Trying to connect to S3 server...")
        s3 = boto3.client(
            's3',
            aws_access_key_id=config_conn["aws_access_key_id"],
            aws_secret_access_key=config_conn["aws_secret_access_key"],
            endpoint_url=config_conn["url"]
        )

        # Initialize buckets list
        buckets = []

        # List all buckets in the S3 account
        buckets_response = s3.list_buckets()
        if "Buckets" in buckets_response:
            buckets = [bucket["Name"] for bucket in buckets_response["Buckets"]]
            print("-> You are connected to S3 server")

    except Exception as error:
        print("-> Error while connecting to S3 server:", error)
        return []

    metadata_obj = []
    try:
        for bucket_name in buckets:
            # Get a list of all objects in the bucket
            objects = s3.list_objects_v2(Bucket=bucket_name)["Contents"]

            # Add metadata info to the object
            bucket_info = {
                "bucket": bucket_name,
                "objects": [{"object": obj["Key"]} for obj in objects]
            }
            metadata_obj.append(bucket_info)
        print("-> Metadata info of S3 server collected.")
    except Exception as e:
        print(f"-> Error: {e}")
    finally:
        return metadata_obj
