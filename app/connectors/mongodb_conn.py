#-------------------------------------------------------------------------------
# Copyright (C) 2024 ITI
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#-------------------------------------------------------------------------------

from config_model import ConfigConn

# Ensure to import only the necessary library for connecting to the storage solution selected
import subprocess
try:
    from pymongo import MongoClient
except ImportError:
    subprocess.run(["pip", "install", "pymongo"])
    from pymongo import MongoClient


###
# Connect to MongoDB based on config_conn params and return metadata_obj
###
def db_conn(config_conn: ConfigConn):
    try:
        print("-> Trying to connect to MongoDB...")
        connection = MongoClient(
            host=config_conn.db_host,
            port=config_conn.db_port,
            authSource=config_conn.db_name,
            username=config_conn.db_user,
            password=config_conn.db_pass,
            authMechanism="SCRAM-SHA-256"  # Use SCRAM-SHA-256 authentication
        )

        # Access the specified database
        db = connection[config_conn.db_name]

        # List all collections in the database
        collections = db.list_collection_names()
        print("-> You are connected to:", config_conn.db_name)

    except Exception as error:
        print("-> Error while connecting to MongoDB:", error)
    finally:
        metadata_obj = []
        if connection:
            try:
                for collection_name in collections:
                    # Get a list of all fields in the collection
                    collection = db[collection_name]
                    fields = collection.find_one()

                    # Add metadata info to the object
                    collection_info = {
                        "collection": collection_name,
                        "fields": [{"field": field, "data_type": type(fields[field]).__name__} for field in fields]
                    }
                    metadata_obj.append(collection_info)
                print("-> Metadata info of MongoDB collected.")
            except Exception as e:
                print(f"-> Error: {e}")
            finally:
                connection.close()
        return metadata_obj
