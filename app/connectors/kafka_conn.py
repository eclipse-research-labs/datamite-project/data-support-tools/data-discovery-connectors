#-------------------------------------------------------------------------------
# Copyright (C) 2024 ITI
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#-------------------------------------------------------------------------------

# Ensure to import only the necessary library for connecting to the storage solution selected
import subprocess
try:
    from confluent_kafka.schema_registry.schema_registry_client import SchemaRegistryClient
except ImportError:
    subprocess.run(["pip", "install", "confluent-kafka"])
    from confluent_kafka.schema_registry.schema_registry_client import SchemaRegistryClient


###
# Connect to Kafka based on config_conn params and return metadata_obj
###
def db_conn(schema_registry_url):
    metadata_obj = []
    try:
        print("-> Trying to connect to Kafka...")

        # Initialize Kafka client
        schema_registry_client = SchemaRegistryClient({"url": schema_registry_url["endpoint"]})

        # Retrieve list of subjects
        subjects = schema_registry_client.get_subjects()

        # Retrieve metadata for each subject
        for subject in subjects:
            metadata = schema_registry_client.get_latest_version(subject)
            metadata_info = {
                "subject": subject,
                "schema": metadata.schema,
                "schema_id": metadata.schema_id,
                "version": metadata.version,
            }
            metadata_obj.append(metadata_info)

        print("-> Metadata from Kafka collected.")

    except Exception as error:
        print("-> Error while connecting to Kafka:", error)

    return metadata_obj
