#-------------------------------------------------------------------------------
# Copyright (C) 2024 ITI
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#-------------------------------------------------------------------------------

import os
import json
from config_model import ConfigConn

def main_process(config_conn: ConfigConn):
    # TODO: Refer to issue #2 for more info

    try:
        db_type = config_conn.db_type
        match db_type:
            case "elasticsearch":
                from connectors import elastic_conn
                result = elastic_conn.db_conn(config_conn)
            case "postgresql":
                from connectors import postgre_conn
                result = postgre_conn.db_conn(config_conn)
            case "mongodb":
                from connectors import mongodb_conn
                result = mongodb_conn.db_conn(config_conn)
            case "s3":
                from connectors import s3_conn
                result = s3_conn.db_conn(config_conn)
            case "virtuoso":
                from connectors import virtuoso_conn
                result = virtuoso_conn.db_conn(config_conn)
            case "kafka":
                from connectors import kafka_conn
                result = kafka_conn.db_conn(config_conn)
            case _:
                result = "-> Error: No db_type value received"

        # Save the results
        if result:
            metadata_folder = "metadata"
            metadata_object = db_type+"_metadata_info.json"
            # Ensure that folder exists
            if not os.path.exists(metadata_folder):
                os.makedirs(metadata_folder)
            result_file_path = os.path.join(metadata_folder, metadata_object)
            with open(result_file_path, "w") as file:
                json.dump(result, file, indent=2)
            print("-> Metadata info saved to json file.")

        # TODO: Refer to issue #6 for more info            
        return result

    except Exception as error:
        print("-> Error in main_process:", error)
        raise Exception(f"Error in main process: {error}")
