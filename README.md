<!--- *************************************************************************
      *Copyright (C) 2024 ITI
      *
      *Permission is hereby granted, free of charge, to any person obtaining a copy
      *of this software and associated documentation files (the "Software"), to deal
      *in the Software without restriction, including without limitation the rights
      *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
      *copies of the Software, and to permit persons to whom the Software is
      *furnished to do so, subject to the following conditions:
      *
      *The above copyright notice and this permission notice shall be included in
      *all copies or substantial portions of the Software.
      *
      *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
      *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
      *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
      *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
      *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
      *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
      *SOFTWARE.
      ************************************************************************* --->

## DESCRIPTION

This project will host the developments for the Data Discovery and Ingestion component, included in the Data Support Tools, as can be seen in the architecture figure below.

![architecture_DDTools](doc/arch_extended_DDTools.png)

Once the connection is established, **extracts all the metadata information into** a _storage_solution_type.json_ **file as a result**.

The main goal is intended to provide **connection to different pilot storage solutions**.

**This is the main flow**:

![main_flow_chart](doc/main_flow_chart.png)

Check the file `architecture_storage_connector.drawio` inside [doc](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/data-discovery-connectors/-/tree/main/doc) folder for further info about the whole functionality.

## INSTALLATION & REQUIREMENTS

Although the main language of the project is Python, it is not necessary to have it installed in order to test the project on your local environment.
For that purpose, **the project uses Docker for a containerized execution**.

Download recommended docker version from [Docker Official Site](https://www.docker.com/get-docker)

## USAGE

1. Download or clone this repository on your local machine.

2. Open a terminal pointing to the downloaded folder of step 1 and run `docker compose up`.

3. Open `http://localhost:8090/docs` on your favorite browser and in the *get_metadata_info* method fill the right values of the intended storage solution on that you want to connect. For example, `elasticsearch` or `postgresql` as value on `db_type` parameter and the proper data connection values on the rest of the parameters.

4. Wait until the process finishes and find the metadata information _.json_ file as a result, inside `/app/metadata` folder. Notice that the file is named with the `db_type` value filled on the config file (for example, `elasticsearch_metadata_info.json`)

Note: If you don't have any storage solution for test the application, you can use samples inside `devdb` folder. In that folder you find specific `README.md` file with further information.

## ROADMAP

Future versions of this project has to resolve the following points:

1. Resolve pending issues. More info on the [main board](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/data-discovery-connectors/-/boards).

2. Allow connection to the following storage solutions:

- [x] PostgreSQL
- [x] ElasticSearch
- [x] S3 object storage (like MinIO)
- [x] MongoDB
- [x] Kafka
- [x] Virtuoso
- [ ] Cassandra
- [ ] MariaDB
- [ ] Azure Data Lake Storage Gen2
- [ ] MS SQL
- [ ] Redis
- [ ] Arkimet
- [ ] Db-all.e

To do so, the idea is to replicate the script of one of the developed connectors (located on [connectors folder](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/data-discovery-connectors/-/tree/main/app/connectors)) and adapt the code to the storage solution that has to connect.

Take into account that this new connector has to have a simple `db_type` identifier to be used on the [main script](https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-support-tools/data-discovery-connectors/-/blob/main/app/main.py).
On that script, pay attention to the selector of connectors and add the new piece of code that points to the new connector script.

For example, this is the code for elasticsearch on the main script:

    match db_type:
        case "elasticsearch":
            from connectors import elastic_conn
            result = elastic_conn.db_conn(config_conn)
