<!--- *************************************************************************
      *Copyright (C) 2024 ITI
      *
      *Permission is hereby granted, free of charge, to any person obtaining a copy
      *of this software and associated documentation files (the "Software"), to deal
      *in the Software without restriction, including without limitation the rights
      *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
      *copies of the Software, and to permit persons to whom the Software is
      *furnished to do so, subject to the following conditions:
      *
      *The above copyright notice and this permission notice shall be included in
      *all copies or substantial portions of the Software.
      *
      *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
      *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
      *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
      *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
      *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
      *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
      *SOFTWARE.
      ************************************************************************* --->

# DATA DISCOVERY CONNECTOS - DEVDB FOLDER

This folder is intended for development purposes with simple dockerized databases and sample data.

Here you have the steps to use:

1. Run the "docker-compose.yml" file (suggestion: docker compose up -d) to have ElasticSearch and PostgreSQL dockerized databases.
2. Inside each db folder you have the connection values ("config_conn_XXX.json" file) for the "config_conn.json" file inside the app folder.

IMPORTANT NOTE: For ElasticSearch database, additional step is required. 
Run "./elastic/sample_db_elastic.sh" file to create index and populate data 
Note: Use "$dos2unix ./elastic/sample_db_elastic.sh" first if you are facing the error "/bin/bash^M: bad interpreter: No such file or directory/"
