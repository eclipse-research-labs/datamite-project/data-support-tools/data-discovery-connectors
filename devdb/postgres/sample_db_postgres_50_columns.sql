-- =============================================================================
-- Copyright (C) 2024 ITI
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- =============================================================================

-- Create tables
-- Table: Suppliers (must be created first)
CREATE TABLE suppliers (
    supplier_id SERIAL PRIMARY KEY,
    company_name VARCHAR(255) NOT NULL,
    contact_name VARCHAR(100),
    contact_email VARCHAR(100),
    contact_phone VARCHAR(15),
    address TEXT,
    city VARCHAR(100),
    state VARCHAR(100),
    zip_code VARCHAR(10),
    country VARCHAR(100),
    contract_start_date DATE,
    contract_end_date DATE,
    supplier_rating NUMERIC(3, 2),
    preferred BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Table: Products (now correctly referencing suppliers)
CREATE TABLE products (
    product_id SERIAL PRIMARY KEY,
    product_name VARCHAR(255) NOT NULL,
    description TEXT,
    price NUMERIC(10, 2) NOT NULL,
    stock_quantity INT DEFAULT 0,
    category VARCHAR(100),
    sku VARCHAR(50) UNIQUE,
    weight NUMERIC(5, 2),
    dimensions VARCHAR(50),
    supplier_id INT,
    color VARCHAR(50),
    material VARCHAR(100),
    warranty_period VARCHAR(50),
    energy_rating VARCHAR(10),
    is_discontinued BOOLEAN DEFAULT FALSE,
    discount_percentage NUMERIC(5, 2),
    cost_price NUMERIC(10, 2),
    tax_rate NUMERIC(5, 2),
    min_order_quantity INT DEFAULT 1,
    max_order_quantity INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (supplier_id) REFERENCES suppliers(supplier_id)
);

-- Table: Customers
CREATE TABLE customers (
    customer_id SERIAL PRIMARY KEY,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(100) UNIQUE NOT NULL,
    phone_number VARCHAR(15),
    address TEXT,
    city VARCHAR(100),
    state VARCHAR(100),
    zip_code VARCHAR(10),
    country VARCHAR(100),
    date_of_birth DATE,
    gender VARCHAR(10),
    account_status VARCHAR(50) DEFAULT 'active',
    signup_date DATE DEFAULT CURRENT_DATE,
    last_login TIMESTAMP,
    loyalty_points INT DEFAULT 0,
    preferred_language VARCHAR(50),
    marketing_opt_in BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Table: Orders
CREATE TABLE orders (
    order_id SERIAL PRIMARY KEY,
    customer_id INT NOT NULL,
    order_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    status VARCHAR(50) NOT NULL,
    shipping_address TEXT,
    billing_address TEXT,
    total_amount NUMERIC(10, 2) NOT NULL,
    payment_method VARCHAR(50),
    shipping_method VARCHAR(50),
    tracking_number VARCHAR(100),
    delivery_date DATE,
    order_notes TEXT,
    gift_wrap BOOLEAN DEFAULT FALSE,
    promotional_code VARCHAR(50),
    order_priority VARCHAR(50),
    estimated_delivery_date DATE,
    tax_amount NUMERIC(10, 2),
    shipping_cost NUMERIC(10, 2),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

-- Table: Order Items (with 50 columns)
CREATE TABLE order_items (
    order_item_id SERIAL PRIMARY KEY,
    order_id INT NOT NULL,
    product_id INT NOT NULL,
    quantity INT NOT NULL,
    price NUMERIC(10, 2) NOT NULL,
    discount NUMERIC(5, 2),
    total_price NUMERIC(10, 2) GENERATED ALWAYS AS (quantity * price - COALESCE(discount, 0)) STORED,
    shipping_status VARCHAR(50),
    estimated_arrival TIMESTAMP,
    tracking_number VARCHAR(100),
    packaging_type VARCHAR(50),
    gift_message TEXT,
    custom_note TEXT,
    warranty_claimed BOOLEAN DEFAULT FALSE,
    item_returned BOOLEAN DEFAULT FALSE,
    return_reason VARCHAR(255),
    refund_amount NUMERIC(10, 2),
    is_digital BOOLEAN DEFAULT FALSE,
    download_link TEXT,
    license_key VARCHAR(255),
    subscription BOOLEAN DEFAULT FALSE,
    subscription_period VARCHAR(50),
    subscription_start_date DATE,
    subscription_end_date DATE,
    promotional_discount NUMERIC(5, 2),
    promotional_code VARCHAR(50),
    loyalty_points_earned INT,
    loyalty_points_redeemed INT,
    restocking_fee NUMERIC(5, 2),
    free_gift_included BOOLEAN DEFAULT FALSE,
    product_batch_number VARCHAR(50),
    manufacturing_date DATE,
    expiration_date DATE,
    fragile_item BOOLEAN DEFAULT FALSE,
    hazardous_material BOOLEAN DEFAULT FALSE,
    eco_friendly_packaging BOOLEAN DEFAULT FALSE,
    delivery_confirmation_required BOOLEAN DEFAULT FALSE,
    special_instructions TEXT,
    weight NUMERIC(5, 2),
    volume NUMERIC(5, 2),
    length NUMERIC(5, 2),
    width NUMERIC(5, 2),
    height NUMERIC(5, 2),
    temperature_sensitive BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
);

-- Table: Categories
CREATE TABLE categories (
    category_id SERIAL PRIMARY KEY,
    category_name VARCHAR(100) NOT NULL UNIQUE,
    description TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Table: Product Categories (many-to-many relationship)
CREATE TABLE product_categories (
    product_id INT NOT NULL,
    category_id INT NOT NULL,
    PRIMARY KEY (product_id, category_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id),
    FOREIGN KEY (category_id) REFERENCES categories(category_id)
);

-- Table: Payments
CREATE TABLE payments (
    payment_id SERIAL PRIMARY KEY,
    order_id INT NOT NULL,
    payment_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    payment_amount NUMERIC(10, 2) NOT NULL,
    payment_method VARCHAR(50) NOT NULL,
    status VARCHAR(50) NOT NULL,
    transaction_id VARCHAR(100),
    currency VARCHAR(10),
    payment_gateway VARCHAR(100),
    payment_notes TEXT,
    FOREIGN KEY (order_id) REFERENCES orders(order_id)
);

-- Create indexes for faster querying (optional)
CREATE INDEX idx_customer_email ON customers(email);
CREATE INDEX idx_order_date ON orders(order_date);
CREATE INDEX idx_product_name ON products(product_name);
CREATE INDEX idx_supplier_company ON suppliers(company_name);

-- Sample data (optional)
INSERT INTO customers (first_name, last_name, email, phone_number, city, state, country) VALUES 
('John', 'Doe', 'john.doe@example.com', '123-456-7890', 'New York', 'NY', 'USA'),
('Jane', 'Smith', 'jane.smith@example.com', '987-654-3210', 'Los Angeles', 'CA', 'USA');

INSERT INTO suppliers (company_name, contact_name, contact_email, contact_phone, city, state, country) VALUES 
('ABC Supplies', 'Tom Johnson', 'tom.johnson@abc.com', '555-1234', 'Chicago', 'IL', 'USA'),
('XYZ Wholesales', 'Emma Davis', 'emma.davis@xyz.com', '555-9876', 'Miami', 'FL', 'USA');

INSERT INTO products (product_name, description, price, stock_quantity, category, sku, weight, dimensions, supplier_id) VALUES 
('Laptop', 'High-performance laptop', 1200.00, 50, 'Electronics', 'SKU12345', 2.5, '14x9x1', 1),
('Smartphone', 'Latest model smartphone', 799.99, 100, 'Electronics', 'SKU67890', 0.3, '6x3x0.5', 2);

INSERT INTO orders (customer_id, status, shipping_address, billing_address, total_amount, payment_method, shipping_method) VALUES 
(1, 'Processing', '123 Main St, New York, NY, USA', '123 Main St, New York, NY, USA', 1200.00, 'Credit Card', 'UPS'),
(2, 'Shipped', '456 Elm St, Los Angeles, CA, USA', '456 Elm St, Los Angeles, CA, USA', 799.99, 'PayPal', 'FedEx');
