-- =============================================================================
-- Copyright (C) 2024 ITI
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- =============================================================================

-- Create sample bookstore database
CREATE TABLE author (
   author_id SERIAL PRIMARY KEY,
   name VARCHAR(100) NOT NULL,
   age INTEGER,
   time_created TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
   time_updated TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp
);

CREATE TABLE book (
   book_id SERIAL PRIMARY KEY,
   title VARCHAR(100) NOT NULL,
   rating FLOAT(2),
   time_created TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
   time_updated TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
   author_id INTEGER REFERENCES author(author_id) NOT NULL
);

CREATE TABLE store (
   store_id SERIAL PRIMARY KEY,
   name VARCHAR(100) NOT NULL
);

-- Insert sample data
INSERT INTO author (name, age) VALUES
   ('Jane Austen', 90),
   ('Charles Dickens', 25),
   ('Agatha Christie', NULL),
   ('J.K. Rowling', 58),
   ('Mark Twain', 0);

INSERT INTO book (title, rating, author_id) VALUES
   ('Pride and Prejudice', 10, 1),
   ('Great Expectations', 20, 2),
   ('Murder on the Orient Express', 30, 3),
   ('Harry Potter and the Sorcerer''s Stone', 40, 4),
   ('The Adventures of Tom Sawyer', 0, 5);

INSERT INTO store (name) VALUES
   ('Casa del Libro'),
   ('Paris Valencia'),
   ('Libreria Soriano'),
   ('Mystery Books & More'),
   ('Book Haven');
