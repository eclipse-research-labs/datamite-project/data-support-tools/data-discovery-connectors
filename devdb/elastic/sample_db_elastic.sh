#!/bin/bash
#-------------------------------------------------------------------------------
# Copyright (C) 2024 ITI
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#-------------------------------------------------------------------------------

# Wait for Elasticsearch to be ready
ES_URL="http://localhost:9200"
until curl -sS "${ES_URL}/_cat/health?h=status" | grep -q "green\|yellow"; do
  sleep 3
done

printf "SCRIPT START. CREATING 'sample_index' AND POPULATING WITH DUMMY DATA ABOUT ECOMMERCE LOGS...\n"

# Create Index
curl -X PUT "${ES_URL}/sample_index" -H 'Content-Type: application/json' -d '
{
  "settings": {
    "number_of_shards": 1,
    "number_of_replicas": 0
  },
  "mappings": {
    "properties": {
      "timestamp": { "type": "date" },
      "customer_id": { "type": "keyword" },
      "product_name": { "type": "text" },
      "category": { "type": "keyword" },
      "price": { "type": "float" },
      "quantity": { "type": "integer" },
      "total_amount": { "type": "float" },
      "is_successful": { "type": "boolean" }
    }
  }
}'

# Populate with dummy data
curl -X POST "${ES_URL}/sample_index/_doc" -H 'Content-Type: application/json' -d '
{
  "timestamp": "2024-02-15T10:30:00",
  "customer_id": "user123",
  "product_name": "Laptop",
  "category": "Electronics",
  "price": 1200.0,
  "quantity": 1,
  "total_amount": 1200.0,
  "is_successful": true
}'

curl -X POST "${ES_URL}/sample_index/_doc" -H 'Content-Type: application/json' -d '
{
  "timestamp": "2024-02-15T11:45:00",
  "customer_id": "user456",
  "product_name": "Headphones",
  "category": "Accessories",
  "price": 89.99,
  "quantity": 2,
  "total_amount": 179.98,
  "is_successful": true
}'

curl -X POST "${ES_URL}/sample_index/_doc" -H 'Content-Type: application/json' -d '
{
  "timestamp": "2024-02-15T13:15:00",
  "customer_id": "user789",
  "product_name": "Smartphone",
  "category": "Electronics",
  "price": 699.99,
  "quantity": 1,
  "total_amount": 699.99,
  "is_successful": false
}'

printf "\nSCRIPT FINISHED. CHECK RESULTS AT: ${ES_URL}/sample_index/_search\n"
